# cypress-demo

Cypress demo for a job interview. This will not be a long-term maintained project.

07Mar2022<br>
Demo is here: https://gitlab.com/richwyant/cypress-demo/-/blob/main/cypress/integration/va-tests/va-test-example.js


05Mar2022<br>
I'm interviewing at a place that works with this project and it's related ones: https://github.com/department-of-veterans-affairs/vets-website
The goal is to have this demo ready for a job interview the following Monday: 07Mar2022

As of this writing, I'm unfamiliar with the functionality other than installation. This is even incomplete due to not having access to everything as someone just coming in off the street. Currently, I can get vets-website, [content-build](https://github.com/department-of-veterans-affairs/content-build), and [vets-api](https://github.com/department-of-veterans-affairs/vets-api) functional enough locally to run Cypress tests on.

I'm also pretty new to Cypress. While we were experimenting with it at my last employer, I got enough exposure to also get this functional enough to run tests on. At least in the first few commits, the example code will be there. As this gets refined, links to what I actually wrote will be included in this README for convenience.

While I'm not going to know everything about the website or Cypress my Monday, I intend to prove I can autonomously move towards this "productivity" thing I keep hearing the grown-ups talk about. 😮

Now, hire me. Please? I'll buy you lunch. Don't know how that's going to work for a remote job. Maybe squeeze a Jimmy Johns through the Ethernet port or something. 🤷
