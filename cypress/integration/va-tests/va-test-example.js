/// <reference types="Cypress" />

// For purposes of this demo, I'm chalking uncaught
// exceptions to my incomplete local instance of vets-website.
Cypress.on('uncaught:exception', (err, runnable) => {
    return false
})

beforeEach(() => {
    cy.visit('localhost:3001')
    cy.contains(/^VA forms$/).click()
    cy.contains('Learn how to apply online').click()
    cy.get('.wizard-button').click()
    cy.get('#newBenefit-0').click()
})

describe('Applying for a new benefit', () => {

    it('smoke test', () => {
        cy.get('#newBenefit-0').check('yes')
        cy.get('legend').contains(/^Are you a Veteran or service member claiming a benefit based on your own service\?$/)
        cy.get('#serviceBenefitBasedOn-0')
        cy.get('#serviceBenefitBasedOn-1')
    })

    it('retains selection after re-opening wizard', () => {
        cy.get('.wizard-button').click() // close wizard
        cy.get('.wizard-button').click() // repoen wizard
        cy.get('#newBenefit-0').check('yes')
        cy.get('legend').contains(/^Are you a Veteran or service member claiming a benefit based on your own service\?$/)
    })

    it('own service = yes', () => {
        cy.get('#serviceBenefitBasedOn-0').click().check('own')
        cy.get('legend span').contains('Are you claiming a ')
            .within(() => {
                cy.get('strong').contains(/^National Call to Service$/)
            })    
            .should('contain','education benefit? (This is uncommon)')
        cy.get('#nationalCallToService-0')
        cy.get('#nationalCallToService-1')
    })

    it('own service = yes, call to service = yes', () => {
        cy.get('#serviceBenefitBasedOn-0').click()
        cy.get('#nationalCallToService-0').click().check('yes')
        cy.get('.usa-alert-body')
            .within(() => {
                cy.get('h4.usa-alert-heading').contains(/^Are you sure\?$/)
                cy.get('p').contains(/^Are all of the following things true of your service\?$/)
                cy.get('ul>li').should('length', 3)
                    .eq(0).contains('Enlisted under the National Call to Service program,')
                        .within(() => { 
                            cy.get('strong').contains(/^and$/)
                        })
                    .next().contains('Entered service between 10/01/03 and 12/31/07,')
                        .within(() => {
                            cy.get('strong').contains(/^and$/)
                        })
                    .next().contains(/^Chose education benefits$/)
            })    
    })

    it('own service = yes, call to service = no', () => {
        cy.get('#serviceBenefitBasedOn-0').click()
        cy.get('#nationalCallToService-1').click().check('no')
        cy.get('legend span').contains('Are you applying for Veteran Employment Through Technology Education Courses (VET TEC)?')
        cy.get('#vetTecBenefit-0')
        cy.get('#vetTecBenefit-1')
    })

    it('own service = yes, call to service = no, vet tec = yes', () => {
        cy.get('#serviceBenefitBasedOn-0').click()
        cy.get('#nationalCallToService-1').click()
        cy.get('#vetTecBenefit-0').click().check('yes')
        cy.get('legend.legend-label').should('length',4) //i.e., no new radio button sets
        // maybe something's supposed to happen here, but it did not on my instance 
    })

    it('own service = yes, call to service = no, vet tec = no', () => {
        cy.get('#serviceBenefitBasedOn-0').click()
        cy.get('#nationalCallToService-1').click()
        cy.get('#vetTecBenefit-1').click().check('no')
        cy.get('legend').contains(/^Are you applying for the post- 9\/11 GI Bill\?$/)
        cy.get('#post911GIBill-0')
        cy.get('#post911GIBill-1')
    })

    it('own service = yes, call to service = no, vet tec = no, post 911 = yes', () => {
        cy.get('#serviceBenefitBasedOn-0').click()
        cy.get('#nationalCallToService-1').click()
        cy.get('#vetTecBenefit-1').click()
        cy.get('#post911GIBill-0').click().check('yes')
        cy.get('#apply-now-link').click()
        cy.url().should('include','/education/apply-for-benefits-form-22-1990/introduction')
    })

    it('own service = yes, call to service = no, vet tec = no, post 911 = no', () => {
        cy.get('#serviceBenefitBasedOn-0').click()
        cy.get('#nationalCallToService-1').click()
        cy.get('#vetTecBenefit-1').click()
        cy.get('#post911GIBill-1').click().check('no')
        cy.get('legend').contains(/^Are you applying for the post- 9\/11 GI Bill\?$/)
        cy.get('#apply-now-link').click()
        cy.url().should('include','/education/apply-for-education-benefits/application/1990/introduction')
    })

    it('own service = no', () => {
        cy.get('#serviceBenefitBasedOn-1').click().check('other')
        cy.get('legend').contains(/^Is your sponsor deceased, 100% permanently disabled, MIA, or a POW\?$/)
        cy.get('#sponsorDeceasedDisabledMIA-0')
        cy.get('#sponsorDeceasedDisabledMIA-1')
    })

    it('own service = no, deceased = yes', () => {
        cy.get('#serviceBenefitBasedOn-1').click()
        cy.get('#sponsorDeceasedDisabledMIA-0').click().check('yes')
        cy.get('legend.legend-label').should('length',4)
        // it dead ends here, leaving this as a fail
        // in reality, i'd go to a developer and ask why
        // nothing else shows up
    })

    it('own service = no, deceased = no', () => {
        cy.get('#serviceBenefitBasedOn-1').click()
        cy.get('#sponsorDeceasedDisabledMIA-1').click().check('no')
        cy.get('legend').contains(/^Has your sponsor transferred their benefits to you\?$/)
        cy.get('#sponsorTransferredBenefits-0')
        cy.get('#sponsorTransferredBenefits-1')
    })

    it('own service = no, deceased = no, transferred = yes', () => {
        cy.get('#serviceBenefitBasedOn-1').click()
        cy.get('#sponsorDeceasedDisabledMIA-1').click()
        cy.get('#sponsorTransferredBenefits-0').click().check('yes')
        cy.get('legend.legend-label').should('length',5)
        // it dead ends here, leaving this as a fail
        // in reality, i'd go to a developer and ask why
        // nothing else shows up
    })

    it('own service = no, deceased = no, transferred = no', () => {
        cy.get('#serviceBenefitBasedOn-1').click()
        cy.get('#sponsorDeceasedDisabledMIA-1').click()
        cy.get('#sponsorTransferredBenefits-1').click().check('no')
        cy.get('.usa-alert-body')
            .within(() => {
                cy.get('h4').contains(/^Your application can't be approved until your sponsor transfers their benefits.$/) // no use-alert-heading? 🤔
                cy.get("a[href='https://milconnect.dmdc.osd.mil/milconnect/public/faq/Education_Benefits-How_to_Transfer_Benefits']")
                    .contains(/^Instructions for your sponsor to transfer education benefits.$/)
            })
        
    })
    
})